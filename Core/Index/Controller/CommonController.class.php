<?php

namespace Index\Controller;

use Think\Controller;
/**
 * @author dongtian <dongtian@nuaa.edu.cn>
 */
class CommonController extends Controller {
	/**
	 * 共有信息生成
	 * @return [type] [description]
	 */
	public function _initialize(){	
		$submenu_model = D('SubmenuView');
		//选择所有开启导航的菜单
		$this->nav = M('menus')->where('is_nav = 1')->order('nav_order')->select();
		//选择所有开启foot的菜单
		$this->foot = M('menus')->where('is_foot = 1')->order('foot_order')->select();
		//将各个子菜单活跃的第一个菜单项选择出来
		$this->introduce = $submenu_model->where(array('is_active'=>1,'parent_id'=>2))->order('sort_order asc')->find();
		$this->contact = $submenu_model->where(array('is_active'=>1,'parent_id'=>7))->order('sort_order asc')->find();
		$this->news = $submenu_model->where(array('is_active'=>1,'parent_id'=>3))->order('sort_order asc')->find();
		$this->recruit = $submenu_model->where(array('is_active'=>1,'parent_id'=>6))->order('sort_order asc')->find();
		$this->download = $submenu_model->where(array('is_active'=>1,'parent_id'=>5))->order('sort_order asc')->find();
		$this->category = M('categories')->where(array('is_active'=>1))->order('sort_order asc')->find();
		//显示网站信息
		$this->webconfig = M('webconfig')->find();
		//网站图片显示
		$images_model = M('images');
		//选取log图片
		$this->log = $images_model->where('type_id = 1')->find();
		//选取幻灯图片
		$this->backgrounds = $images_model->where('type_id = 2')->select();
		//选取二维码图片
		$this->qrcode = $images_model->where('type_id = 3')->find();
		$GLOBALS['submenu_introduce'] = $this->introduce;
		$GLOBALS['submenu_contact'] = $this->contact;
		$GLOBALS['submenu_news'] = $this->news;
		$GLOBALS['submenu_recruit'] = $this->recruit;
		$GLOBALS['submenu_download'] = $this->download;
		$GLOBALS['category'] = $this->category;
		$GLOBALS['webconfig'] = $this->webconfig;
	}

}
?>
