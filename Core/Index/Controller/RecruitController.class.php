<?php
	namespace Index\Controller;
	use Think\Controller;

	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 人才招聘控制器
	 */
	
	class RecruitController extends CommonController {

		public function view (){

			$submenu_model = D('SubmenuView');
			$id = I('get.type_id','','intval');
			$submenu = $submenu_model->where(array('type_id'=>$id))->find();
			if($submenu['parent_id']!='6'){
				$this->error('管理员尚未开启此菜单项');//防止恶意get传参
			}
			//菜单显示
			$this->menuname = $submenu['name_menu'];

			//子菜单名称显示(title)
			$this->title = $submenu['name'];

			//将该菜单项的全部激活了的子菜单显示出来
			$result = $submenu_model->where(array('is_active'=>1,'parent_id'=>6))->order('sort_order asc')->select();
			if($result){
				$this->submenus = $result;//存在开启的子菜单，则渲染
			}
			if($id == 12){
				//查找该类型子菜单对应的文章类型
				$where['submenu_type_id'] = $id;
				$type_id = M('recruit_article')->where($where)->getField('article_type_id');
				//查找文章
				$article = M('articles')->where(array('type_id' => $type_id,'is_active'=>1))->find();
				$article['content'] = htmlspecialchars_decode(stripslashes($article['content']));
				$this->article = $article;
				$this->submenu_type = $id;
				$this->display();
				die;
			}
			if($id == 13){
				$this->submenu_type = $id;
				$this->display();
				die;
			}
		}

		/**
		 * 表单处理
		 * @return [type] [description]
		 */
		public function handle(){
			if(!IS_POST){
				$this->error('页面不存在');
			}
			$config = array('maxSize' => 3145728,
			'rootPath' => './Public/Uploads/Resumes/',
			'exts' => array('txt', 'doc', 'docx','pdf', 'rar','zip'),
			'subName' => array('date', 'Ymd'),
			);
			$upload = new \Think\Upload($config); // 实例化上传类
			$info = $upload->uploadOne($_FILES['resumeurl']);
			if (!$info) {
				// 上传错误提示错误信息
				$this->error($upload->getError());
			}else {
				// 上传成功 获取上传文件信息
					$resumeurl = $info['savepath'] . $info['savename'];
			}
			$data = I('post.');
			$data['resumeurl'] = $resumeurl;
			$result = M('applicants')->add($data);
			if($result){
				$this->success('我们即刻联系您');
			}else{
				$this->error('数据发送失败');
			}
		}
	}
?>