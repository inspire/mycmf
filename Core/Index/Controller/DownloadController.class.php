<?php
	namespace Index\Controller;
	use Think\Controller;

	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 文件下载 控制器
	 */
	class DownloadController extends CommonController {

		/**
		 * listview页面展示
		 */
		public function listView (){
			$submenu_model = D('SubmenuView');
			$id = I('get.type_id','','intval');
			$submenu = $submenu_model->where(array('type_id'=>$id))->find();
			if($submenu['parent_id']!='5'){
				$this->error('管理员尚未开启此菜单项');//防止恶意get传参
			}
			//菜单显示
			$this->menuname = $submenu['name_menu'];

			//子菜单名称显示(title)
			$this->title = $submenu['name'];

			//将该菜单项的全部激活了的子菜单显示出来
			$result = $submenu_model->where(array('is_active'=>1,'parent_id'=>5))->order('sort_order asc')->select();
			if($result){
				$this->submenus = $result;//存在开启的子菜单，则渲染
			}
			
			//选择该子菜单类型对应的所有文件
			$p = I('get.p',1,'intval');//获取页码的参数
			$type_id = M('download_file')->where(array('submenu_type_id'=>$id))->getField('file_type_id');//找出文件类型
			$file_model = M('files');
			$where['is_active'] = 1;
			$where['type_id'] = $type_id;
			$count = $file_model->count();
			$list = $file_model->where($where)->order('update_time desc')->page($p.',10')->select();
			$Page = new \Think\Page($count,10);//实例化分页类
			$Page->setConfig('header','条记录');
			$Page->setConfig('pre','上一页');
			$Page->setConfig('next','下一页');
			$Page->setConfig('first','第一页');
			$Page->setConfig('last','最后页');
			$this->list = $list;
			$this->page = $Page->show();
			$this->file_type = $type_id;
			$this->display();
		}
	}
?>