<?php
	namespace Index\Controller;
	use Think\Controller;
	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 首页控制器
	 */
	class IndexController extends CommonController {
		public function index(){
			
			$article_model = M('articles');
			$submenu_model = D('SubmenuView');
			//显示推荐产品
			$product_model = D('ProductView');
			$this->products_introduce = $product_model->where(array('is_active'=>1,'is_introduce'=>1,'category_is_active'=>1))->limit(4)->order('update_time desc')->select();

			//显示公司介绍
			$result = $submenu_model->where(array('is_active'=>1,'type_id'=>1))->find();
			if($result){
				//所需文章对应的菜单开启,查找文章
				$article_type = M('introduce_article')->where(array('submenu_type_id'=>$result['type_id']))->getField('article_type_id');
				//只有开启对应菜单才能显示对应文章,公司简介文章只有一天记录
				$this->article_introduce = $article_model->where(array('is_active'=>1,'type_id'=>$article_type))->getField('content');
			}

			//新闻动态显示
			$news_article_model = M('news_article');
			//专题报道
			$result = $submenu_model->where(array('is_active'=>1,'type_id'=>6))->find();
			if($result){
				//对应菜单激活，查询对应的文章
				$article_type = $news_article_model->where(array('submenu_type_id'=>$result['type_id']))->getField('article_type_id');
				$this->submenu_special = $result;
				$this->articles_special = $article_model->where(array('is_active'=>1,'type_id'=>$article_type))->limit(4)->order('update_time desc')->select();
			}
			//公司新闻
			$result = $submenu_model->where(array('is_active'=>1,'type_id'=>7))->find();
			if($result){
				//对应菜单激活，查询对应的文章
				$article_type = $news_article_model->where(array('submenu_type_id'=>$result['type_id']))->getField('article_type_id');
				$this->submenu_company = $result;
				$this->articles_company = $article_model->where(array('is_active'=>1,'type_id'=>$article_type))->limit(7)->order('update_time desc')->select();
			}
			//行业新闻
			$result = $submenu_model->where(array('is_active'=>1,'type_id'=>8))->find();
			if($result){
				//对应菜单激活，查询对应的文章
				$article_type = $news_article_model->where(array('submenu_type_id'=>$result['type_id']))->getField('article_type_id');
				$this->submenu_industy = $result;
				$this->articles_industy = $article_model->where(array('is_active'=>1,'type_id'=>$article_type))->limit(7)->order('update_time desc')->select();
			}

			//最新通知显示
			$result = $submenu_model->where(array('is_active'=>1,'type_id'=>9))->find();
			if($result){
				//对应菜单激活，查询对应的文章
				$article_type = $news_article_model->where(array('submenu_type_id'=>$result['type_id']))->getField('article_type_id');
				$this->submenu_notice = $result;
				$this->articles_notice = $article_model->where(array('is_active'=>1,'type_id'=>$article_type))->limit(4)->order('update_time desc')->select();
			}
			
			//显示产品中心界面
			$this->products = $product_model->where(array('category_is_active'=>1,'is_active'=>1))->order('update_time desc')->limit(5)->select();
			$this->category = $GLOBALS['category'];
			$this->submenu_introduce = $GLOBALS['submenu_introduce'];
			$this->submenu_contact = $GLOBALS['submenu_contact'];
			$this->submenu_download = $GLOBALS['submenu_download'];
			$this->display();
		}


		// 404页面
		public function _404() {
			$this->display();
		} 

		/**
		 * 搜索功能
		 */
		public function result (){
			$data = I('post.title');
			$where['name'] = array("like","%$data%");
			$product_model = M('products');
			$id = $product_model->where($where)->getField('id');
			$this->redirect('Index/Product/view',array('id'=>$id));
		}
	}
?>