<?
	namespace Index\Controller;
	use Think\Controller;
	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 产品控制器
	 */
	class ProductController extends CommonController {
		public function listView(){
			//显示menu名称
			$this->menuname = M('menus')->where(array('type_id = 4'))->getField('name');
			//参数，用于分页
			$p = I('get.p', 1, 'intval');
			//一个菜单的一个子菜单的type_id，是固定的
			$id = I('get.type_id','','intval');
			$category_model = M('categories');
			$count = $category_model->count();
			if($id>$count){
				$this->error('管理员尚未开启此菜单项');
			}
			//显示产品中心页面开启的全部子菜单项目
			$where['is_active'] = 1;
			//显示标题
			$this->title = $category_model->where(array('id'=>$id))->getField('name');
			$result = $category_model->where($where)->order('sort_order asc')->select();
			//存在开启的子菜单，则渲染
			if($result){
				$this->submenus = $result;
			}
			//分类显示产品,指定类型，激活的产品查找
			$where_product['is_active'] = 1;
			$where_product['type_id'] = $id;
			
			$product_model = M('products');
			//计算总数
			$count = $product_model->where($where_product)->count();
			//分类查询
			$list = $product_model->where($where_product)->order('update_time desc')->page($p . ',6')->select(); 
			//实例化分页类
			$Page = new \Think\Page($count, 6); 
			// 传入总记录数和每页显示的记录数
			$Page->setConfig('header', '条记录');
			$Page->setConfig('prev', '上一页');
			$Page->setConfig('next', '下一页');
			$Page->setConfig('first', '第一页');
			$Page->setConfig('last', '最后页');
			$this->list = $list;
			$this->page = $Page->show(); // 分页显示输出
			$this->display();
		}

		/**
		 * 单个产品页面展示
		 * @return [type] [description]
		 */
		public function view(){
			//查找产品所属的种类
			//参数get.id是产品的id
			$id = I('get.id','','intval');
			$map['id'] = $id;
			$product = M('products')->where($map)->find();
			$where['id'] = $product['type_id'];
			$this->title = M('categories')->where($where)->getField('name');
			$result = M('categories')->where(array('is_active'=>1))->order('sort_order asc')->select();
			//存在开启的子菜单，则渲染
			if($result){
				$this->submenus = $result;
			}
			$this->menuname = M('menus')->where(array('type_id'=>4))->getField('name');
			$this->product = $product;
			$this->display();
		}
	}
?>