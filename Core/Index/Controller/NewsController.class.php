<?php
	namespace Index\Controller;
	use Think\Controller;

	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 *	新闻资讯控制器
	 * 
	 */
	class NewsController extends CommonController{
		
		/**
		 * 根据子菜单类型显示不同的介绍
		 * @return [type] [description]
		 */
	public function listView (){

		$submenu_model = D('SubmenuView');
		$id = I('get.type_id','','intval');
		$submenu = $submenu_model->where(array('type_id'=>$id))->find();
		if($submenu['parent_id']!='3'){
			$this->error('管理员尚未开启此菜单项');//防止恶意get传参
		}
		//菜单显示
		$this->menuname = $submenu['name_menu'];

		//子菜单名称显示(title)
		$this->title = $submenu['name'];

		//将该菜单项的全部激活了的子菜单显示出来
		$result = $submenu_model->where(array('is_active'=>1,'parent_id'=>3))->order('sort_order asc')->select();
		if($result){
			$this->submenus = $result;//存在开启的子菜单，则渲染
		}
		//查找该类型子菜单对应的文章类型
		$map['submenu_type_id'] = $id;
		$type_id = M('news_article')->where($map)->getField('article_type_id');
		//查找文章
		$where_article['is_active'] = 1;
		$where_article['type_id'] = $type_id;
		//指定类型，激活的文章查找
		$article_model = M('articles');
		//计算总数
		$count = $article_model->where($where_article)->count();
		//分类查询
		$list = $article_model->where($where_article)->order('update_time desc')->page($p . ',15')->select(); 
		//实例化分页类
		$Page = new \Think\Page($count, 15); 
		// 传入总记录数和每页显示的记录数
		$Page->setConfig('header', '条记录');
		$Page->setConfig('prev', '上一页');
		$Page->setConfig('next', '下一页');
		$Page->setConfig('first', '第一页');
		$Page->setConfig('last', '最后页');
		$this->list = $list;
		$this->page = $Page->show(); // 分页显示输出
		$this->display();
	}


	public function view(){
		//查找文章所属的子菜单
		//参数get.id是文章的id
		$id = I('get.id','','intval');
		$map['id'] = $id;
		$article = M('articles')->where($map)->find();
		$where['article_type_id'] = $article['type_id'];
		$submenu_type_id = M('news_article')->where($where)->getField('submenu_type_id');
		$submenu_model = D('SubmenuView');
		$this->title  = $submenu_model->where(array('type_id'=>$submenu_type_id))->getField('name');
		$this->menuname = $submenu_model->where(array('type_id'=>$submenu_type_id))->getField('name_menu');
		$result = $submenu_model->where(array('is_active'=>1,'parent_id'=>3))->order('sort_order asc')->select();
		//存在开启的子菜单，则渲染
		if($result){
			$this->submenus = $result;
		}
		$article['content'] = htmlspecialchars_decode(stripslashes($article['content']));
		$this->article = $article;
		$this->display();
	}
}
	

?>