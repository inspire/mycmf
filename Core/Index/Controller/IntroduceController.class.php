<?php
	namespace Index\Controller;
	use Think\Controller;

	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 公司介绍控制器
	 * 
	 */
	class IntroduceController extends CommonController{
		
		/**
		 * 根据子菜单类型显示不同的介绍
		 * @return [type] [description]
		 */
	public function view (){

		$submenu_model = D('SubmenuView');
		$id = I('get.type_id','','intval');
		$submenu = $submenu_model->where(array('type_id'=>$id))->find();
		if($submenu['parent_id']!='2'){
			$this->error('管理员尚未开启此菜单项');//防止恶意get传参
		}
		//菜单显示
		$this->menuname = $submenu['name_menu'];
		//子菜单名称显示(title)
		$this->title = $submenu['name'];
		//将该菜单项的全部激活了的子菜单显示出来
		$result = $submenu_model->where(array('is_active'=>1,'parent_id'=>2))->order('sort_order asc')->select();
		if($result){
			$this->submenus = $result;//存在开启的子菜单，则渲染
		}
		//查找该类型子菜单对应的文章类型
		$where['submenu_type_id'] = $id;
		$type_id = M('introduce_article')->where($where)->getField('article_type_id');
		//查找文章
		$this->article = M('articles')->where(array('type_id' => $type_id,'is_active'=>1))->getField('content');
		$this->display();
	}

}
	

?>