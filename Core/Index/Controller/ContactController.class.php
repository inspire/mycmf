<?php
	namespace Index\Controller;
	use Think\Controller;

	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 联系菜单 控制器
	 */
	class ContactController extends CommonController {

		public function view(){
			$submenu_model = D('SubmenuView');
			$id = I('get.type_id','','intval');
			$where['type_id'] = $id;
			$submenu = $submenu_model->where($where)->find();
			if($submenu['parent_id']!='7'){
				$this->error('管理员尚未开启此菜单项');//防止恶意get传参
			}
			//菜单显示
			$this->menuname = $submenu['name_menu'];

			//子菜单名称显示(title)
			$this->title = $submenu['name'];

			//将该菜单项的全部激活了的子菜单显示出来
			$result = $submenu_model->where(array('is_active'=>1,'parent_id'=>7))->order('sort_order asc')->select();
			if($result){
				$this->submenus = $result;//存在开启的子菜单，则渲染
			}
			$this->submenu_type = $id;
			$this->display();
		}

		/**
		 * 表单处理
		 */
		public function handle(){
			if(!IS_POST){
				$this->error('页面不存在');
			}
			$data = I('post.');
			$result = M('customers')->add($data);
			if($result){
				$this->success('我们即刻联系您');
			}else{
				$this->error('数据发送失败');
			}
		}
	}

?>