<?php

namespace Index\Model;

use Think\Model\ViewModel;

/**
 * @author dongtian <dongtian@nuaa.edu.cn>
 * 产品与种类视图
 */
class ProductViewModel extends ViewModel {
	public $viewFields = array(
		'products' => array('id','type_id', 'is_active','is_introduce','product_num', 'name', 'update_time','cas_num', 'cargo_num', 'standard', 'pack', 'unit', 'list_price', 'imageurl'),
		'categories' => array('id' => 'category_id', 'name' => 'category_name','is_active'=> 'category_is_active', '_on' => 'products.type_id=categories.id'),
		);
}

?>
