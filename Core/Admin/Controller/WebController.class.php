<?php

/**
 * 网站基本设置控制器
 * 
 * @author dongtian@nuaa.edu.cn
 * @access public 
 */

namespace Admin\Controller;

use Think\Controller;

class WebController extends CommonController {
	/**
	 * webconfig中有且仅有一条记录
	 * @return [type] [description]
	 */
	public function view() {
		$webconfig = M('webconfig');
		$this->title = "网站信息配置";
		$this->web = $webconfig->find();
		$this->display();
	} 
	// 修改执行
	public function editDo() {
		$webconfig = M('webconfig');
		if (IS_POST) {
			$data = I();
			//因为仅仅保留一条数据，所以可以使用id
			$data['id'] = 1;
			$result = $webconfig->save($data);
			if($result){
				$this->success('修改成功');
			}else{
				$this->error('修改失败，请联系管理员');
			}
		}else{//防止恶意访问
			$this->error("页面不存在");
		}
	}
}
