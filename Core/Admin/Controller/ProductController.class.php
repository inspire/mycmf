<?php

/**
 * 产品控制器
 * @author dongtian <dongtian@nuaa.edu.cn>
 * @access public 
 */

namespace Admin\Controller;

use Think\Controller;

class ProductController extends CommonController {
	
	/**
	 * 产品列表
	 * @return [type] [description]
	 */
	public function listView() {
		$this->title = "产品列表"; 
		$product_model = D("ProductRelation");
		$p = I('get.p', 1, 'intval');
		$list = $product_model->relation("product_type")->page($p . ',8')->select();
		$count = $product_model->count(); // 查询满足要求的总记录数
		$Page = new \Think\Page($count, 8); // 实例化分页类 传入总记录数和每页显示的记录数
		$Page->setConfig('header', '个产品');
		$Page->setConfig('prev', '上一页');
		$Page->setConfig('next', '下一页');
		$Page->setConfig('first', '第一页');
		$Page->setConfig('last', '最后页');
		$show = $Page->show(); // 分页显示输出
		$this->products = $list; // 赋值
		$this->page = $show; // 赋值分页输出
		
		$this->display();
	}

	/**
	 * 产品详情
	 * @return [type] [description]
	 */
	public function view() {
		$this->title = "产品详情"; 
		// 取得参数
		$id = I('get.id', '', 'intval');
		$map['id'] = $id;
		$product_model = D("ProductRelation");
		$this->product = $product_model->relation("product_type")->where($map)->find();
		$this->display();
	}
	
	/**
	 * 添加产品视图
	 */
	public function add() {
		$this->title = "新增产品";
		$this->categories = M('categories')->select();
		$this->display();
	} 
	/**
	 * 执行添加
	 */
	public function addDo() {
		$config = array('maxSize' => 3145728,
			'rootPath' => './Public/Uploads/Products/',
			'exts' => array('jpg', 'gif', 'png', 'jpeg'),
			'subName' => array('date', 'Ymd'),
			);
		//有文件上传
		if($_FILES['imageurl']['name'] != null)	{

			$upload = new \Think\Upload($config); // 实例化上传类
			$info = $upload->uploadOne($_FILES['imageurl']);
			if (!$info) {
				// 上传错误提示错误信息
				$this->error($upload->getError());
			}else {
				// 上传成功 获取上传文件信息
				$imageurl = $info['savepath'] . $info['savename'];
			}
		//如果上传文件为空，则选择默认的图片
		}else{
				$imageurl = 'default.jpg';
		}

		//修改上传图片的大小
		$imagename = './Public/Uploads/Products/' . $imageurl;
		//实例化图像处理类
		$image = new \Think\Image();
		$image->open($imagename);
		$image->thumb(250,183,\THink\Image::IMAGE_THUMB_FIXED);
		$image->save($imagename);

		$data = $_POST;
		$data['imageurl'] = $imageurl;
		$data['publish_time'] = date("Y-m-d H:i:s");
		$data['update_time'] = date("Y-m-d H:i:s");
		$data['author'] = I('session.username');

		$product_model = M('products');
		$result = $product_model->add($data);
		if ($result) {
			$this->success("添加成功", U('Admin/Product/listView'));
		}else {
			$this->error("添加错误");
		}
	}
		
	/**
	 * 产品修改视图
	 * @return [type] [description]
	 */
	public function edit() {
		$this->title = "编辑产品"; 
		// 取得参数
		$id = I('id', '', 'intval');
		$map['id'] = $id;
		$product_model = D('ProductRelation');
		$product = $product_model->relation('product_type')->where($map)->find();
		if($product){
			$this->product = $product;	
			$this->display();	
		}else{
			$this->error('该产品不存在');
		}
	} 

	
	/**
	 * 修改执行
	 * @return [type] [description]
	 */
	public function editDo() {
		$product_model = M('products');
		$id = I('post.id', '', 'intval');
		$map['id'] = $id;
		$data = $_POST;
		// 如果有新图片，上传新图片，删除旧图片
		if ($_FILES['imageurl']['name']) {
			// 删除旧的图片
			$imageurl = $product_model->where($map)->getField('imageurl');
			$file_name = "./Public/Uploads/Products/" . $imageurl; 
			// 删除
			if($imageurl != "default.jpg"){
				unlink($file_name);
			}
			$config = array('maxSize' => 3145728,
				'rootPath' => './Public/Uploads/Products/',
				'exts' => array('jpg', 'gif', 'png', 'jpeg'),
				'subName' => array('date', 'Ymd'),
				);
			$upload = new \Think\Upload($config); // 实例化上传类
			
			$info = $upload->uploadOne($_FILES['imageurl']);
			if (!$info) {
				// 上传错误提示错误信息
				$this->error($upload->getError());
			}else {
				// 上传成功 获取上传文件信息
				$imageurl = $info['savepath'] . $info['savename'];
				$data['imageurl'] = $imageurl;
				//处理图像大小
				$imagename = './Public/Uploads/Products/' . $imageurl;
				//实例化图像处理类
				$image = new \Think\Image();
				$image->open($imagename);
				$image->thumb(250,183,\THink\Image::IMAGE_THUMB_FIXED);
				$image->save($imagename);
			}
		}

		$data['update_time'] = date("Y-m-d");
		$data['author'] = I('session.username');
		$result = $product_model->where($map)->save($data);
		if ($result) {
			$this->success("更新成功", U('Admin/Product/view', array('id' => $id)));
		}else {
			$this->error("数据更新错误，请联系管理员");
		}
	}

	/**
	 * 产品删除
	 * @return [type] [description]
	 */
	public function delete() {
		// 获得参数
		$id = I('id', '', 'intval');
		$product_model = M('products');
		$map['id'] = $id;
		$imageurl = $product_model->where($map)->getField('imageurl');
		if ($imageurl) {
			$file_name = "./Public/Uploads/Products/" . $imageurl;
			if($imageurl != "default.jpg"){
				unlink($file_name);
			}
		}

		$result = $product_model->where($map)->delete();
		if ($result) {
			$this->success("删除成功", U('Admin/Product/listView'));
		}else {
			$this->error("删除失败");
		}
	}
}
