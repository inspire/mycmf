<?php
	
namespace Admin\Controller;
use Think\Controller;

/**
 * @author dongtian <dongtian@nuaa.edu.cn>
 * 下载资料管理控制器
 */
 class DownloadController extends CommonController
 {
 		
 		/**
 		 * 所有下载信息的显示输出
 		 * @return [type] [description]
 		 */
 		public function listView(){
 			$this->title = '下载资料信息列表';
 			$file_model = D('FileRelation');
 			$p = I('get.p',1,'intval');
 			$list = $file_model->relation('file_type')->page($p . ',8')->select();
 			$count = $file_model->count();

 			//实例化分页类
 			$Page = new \Think\Page($count,8);
 			$Page->setConfig('header', '个应聘者');
			$Page->setConfig('prev', '上一页');
			$Page->setConfig('next', '下一页');
			$Page->setConfig('first', '第一页');
			$Page->setConfig('last', '最后页');
			$show = $Page->show(); // 分页显示输出
			$this->files = $list; // 赋值
			$this->page = $show; // 赋值分页输出
			
			$this->display();
 		}

 		/**
 		 * 下载资料详情
 		 * @return [type] [description]
 		 */
 		public function view(){
 			$this->title = '下载资料信息';
 			$id = I('id', '', 'intval');
			$map['id'] = $id;
			$file_model = D('FileRelation');
			$this->file = $file_model->relation('file_type')->where($map)->find();
			$this->display();
 		}
 		/**
 		 * 添加下载资料信息
 		 */
 		public function add(){
 			$this->title = '添加下载资料信息';
 			$this->types = M('types_file')->select();
 			$this->display();
 		}
 		/**
 		 * 执行添加
 		 */
 		public function addDo(){
 			$file_model = M('files');
 			$data = I('post.');
 			$data['create_time'] = date("Y-m-d H:i:s");
			$data['update_time'] = date("Y-m-d H:i:s");
 			$config = array('maxSize' => 930456592,
			'rootPath' => './Public/Uploads/Files/',
			'exts' => array('doc', 'docx', 'pdf', 'txt','rar','zip'),
			'subName' => array('date', 'Ymd'),
			);
			$upload = new \Think\Upload($config); // 实例化上传类
			$info = $upload->uploadOne($_FILES['fileurl']);
			if (!$info) {
				// 上传错误提示错误信息
				$this->error($upload->getError());
			}else {
				// 上传成功 获取上传文件信息
				$fileurl = $info['savepath'] . $info['savename'];
				$data['fileurl'] = $fileurl;
			}
 			$result = $file_model->add($data);
 			if($result){
 				$this->success("添加成功", U('Admin/Download/listView'));
 			}else{
 				$this->error('添加失败');
 			}
 		}
 		/**
 		 * 顾客信息修改
 		 * @return [type] [description]
 		 */
 		public function edit(){
 			$this->title = '修改下载资料信息';
 			$id = I('get.id','','intval');
 			$map['id'] = $id;
 			$this->file = D('FileRelation')->relation('file_type')->where($map)->find();
 			$this->display();
 		}
 		/**
 		 * 执行修改
 		 * @return [type] [description]
 		 */
 		public function editDo(){
 			$id = I('post.id','','intval');
 			$map['id'] = $id;
 			$data = $_POST;
 			$data['update_time'] = date("Y-m-d H:i:s");
 			$file_model = D('FileRelation');
 			$config = array('maxSize' => 3145728,
			'rootPath' => './Public/Uploads/Files/',
			'exts' => array('doc', 'docx', 'pdf', 'txt','rar','zip'),
			'subName' => array('date', 'Ymd'),
			);
			//有文件上传
			if($_FILES['fileurl']['name'] != null)	{

				$upload = new \Think\Upload($config); // 实例化上传类
				$info = $upload->uploadOne($_FILES['fileurl']);
				if (!$info) {
					// 上传错误提示错误信息
					$this->error($upload->getError());
				}else {
					//删除旧的资料
					$fileurl = $file_model->where($map)->getField('fileurl');
					$file_name = "./Public/Uploads/Files/" . $fileurl; 
					unlink($file_name);
					// 上传成功 获取上传文件信息
					$fileurl = $info['savepath'] . $info['savename'];
					$data['fileurl'] = $fileurl;
				}
			}
 			$result = $file_model->save($data);
 			if($result){
 				$this->success("修改成功", U('Admin/Download/listView'));
 			}else{
 				$this->error('修改失败');
 			}
 		}
 		/**
 		 * 下载资料信息删除
 		 * @return [type] [description]
 		 */
 		public function delete(){
 			$id = I('id','','intval');
 			$where['id'] = $id;
 			$file_model = M('files');
 			$fileurl = $file_model->where($where)->getField('fileurl');
			if ($fileurl) {
				$file_name = "./Public/Uploads/Files/" . $fileurl;
					unlink($file_name);
			}
 			$result = $file_model->where($where)->delete();
 			if($result){
 				$this->success("删除成功", U('Admin/Download/listView'));
 			}else{
 				$this->error('删除失败');
 			}
 		}	
 } 

?>