<?php
	namespace Admin\Controller;
	use Think\Controller;

	/**
	 * 文章控制器
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 */
	class ArticleController extends CommonController {

		/**
		 *文章列表展示
		 */
		public function listView(){
			$article_model = D('ArticleRelation');
			$p = I('get.p',1,'intval');//文章页码参数
			$count = $article_model->count();
			$articles = $article_model->relation('article_type')->page($p.',8')->select();
			$Page = new \Think\Page($count,8);
			$Page->setConfig('header','片文章');
			$Page->setConfig('prev','上一页');
			$Page->setConfig('next','下一页');
			$Page->setConfig('first','第一页');
			$Page->setConfig('last','最后页');
			$page = $Page->show();
			$this->title = '文章列表';
			$this->page = $page;
			$this->articles = $articles;
			$this->display();
		}

		/**
		 * 文章详情显示
		 */
		public function view(){
			$this->title = '文章详情';
			$id = I('get.id','','intval');
			$map['id'] = $id;
			$this->article = D('ArticleRelation')->relation('article_type')->where($map)->find();
			$this->display();
		}

		/**
		 * 添加文章
		 */
		public function add(){
			$this->title = '添加文章';
			$this->types_article = M('types_article')->select();
			$this->display();
		}

		/**
		 * 执行添加,确保特定类型的文章只有一条
		 */
		public function addDo(){
			$article_model = M('articles');
			$data = I('post.');
			$data['publish_time'] = date('Y-m-d H:i:s');
			$data['update_time'] = date('Y-m-d H:i:s');
			$where['type_id'] = $data['type_id'];
			$result = $article_model->where($where)->find();
			if($result){
				//存在对应类型的文章
				switch ($data['type_id']) {
					//只允许一条记录的文章类型
					case '1';
					case '2';
					case '3';
					case '4';
					case '5';
					case '10':
						$this->error('该类型文章只可添加一篇');
						break;
					//可以随意添加的文章类型
					case '6';
					case '7';
					case '8';
					case '9':
						$result = $article_model->add($data);
						if($result){
							$this->success('添加成功',U('Admin/Article/listView'));
						}else{
							$this->error('添加失败');
						}
						break;
					default:
						$this->error('添加失败，该文章类型不存在');
						break;
				}
			}else{
				//不存在对应类型文章,直接添加
				$result = $article_model->add($data);
				if($result){
					$this->success('添加成功',U('Admin/Article/listView'));
				}else{
					$this->error('添加失败');
				}
			}	
		}


		/**
		 * 编辑文章视图
		 */
		public function edit(){
			$this->title = '编辑文章';
			$id = I('get.id','','intval');//文章的id
			$map['id'] = $id;
			$article_model = D('ArticleRelation');
			$result = $article_model->relation('article_type')->where($map)->find();
			if(!$result){
				$this->error('您要编辑的文章不存在');
			}else{
				$this->article = $result;
				$this->display();
			}
		}

		/**
		 * 执行编辑
		 */
		public function editDo(){
			$data = I('post.');
			$data['update_time'] = date('Y-m-d H:i:s');
			$result = M('articles')->save($data);
			if($result){
				$this->success('修改成功',U('Admin/Article/listView'));
			}else{
				$this->error('修改失败');
			}
		}

		/**
		 * 删除文章
		 */
		public function delete(){
			$id = I('get.id','','intval');
			$map['id'] = $id;
			$result = M('articles')->where($map)->delete();
			if($result){
				$this->success('删除成功',U('Admin/Article/listView'));
			}else{
				$this->error('删除失败');
			}
		}
	}

?>