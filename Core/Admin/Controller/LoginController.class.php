<?php

namespace Admin\Controller;

use Think\Controller;
use Org\Util\Rbac;

/**
 * Description of LoginController
 * 
 * @author Administrator 
 */
class LoginController extends Controller {
	// put your code here
	public function index() {
		$this->display();
	}

	public function login() {
		if (!IS_POST)
			E('页面不存在'); 

		$username = I('username');
		$pwd = I('password', '', 'md5');
		$code = I('code'); 
		//检验验证码
		$verfity = $this->check_verify($code, '');
		if (!$verfity) {
			$this->error('验证码错误');
		} 
		
		// 检测用户名和密码
		$user = M('users')->where(array('name' => $username))->find();
		 if (!$user || $user['password'] != $pwd) {
		 	$this->error('用户名或密码错误');
		 }
		  
		if ($user['is_active']) {
			$this->error('用户被锁定');
		} 
		// 
		$data = array('id' => $user['id'],
			'last_login' => time(),
			'logins' => $user['logins'] + 1,
			);
		M('users')->save($data); 

		session(C('USER_AUTH_KEY'), $user['id']);
		session('username', $user['name']);
		session('logintime', date('Y-m-d H:i:s'), $user['last_login']); 

		if ($user['name'] == C('RBAC_SUPERADMIN')) {
			session(C('ADMIN_AUTH_KEY'), true);
		} 

		new \Org\Util\Rbac();
		Rbac::saveAccessList(); 

		$this->redirect('Admin/Index/index');
	} 

	public function verify() {
		$config = array('fontSize' => 20, 
			'length' => 4, 
			'useNoise' => false, 
			);
		ob_clean();ob_clean();
		$Verify = new \Think\Verify($config);
		$Verify->codeSet = '0123456789';
		$Verify->entry();
	} 
	
	function check_verify($code, $id = '') {
		$verify = new \Think\Verify();
		return $verify->check($code, $id);
	}
}

?>
