<?php

/**
 * Description of CommenController
 * 
 * @author jackson 
 */

namespace Admin\Controller;

use Think\Controller;
use Org\Util\Rbac;

class CommonController extends Controller {
	/**
	 * 基础验证登陆
	 */
	public function _initialize() {
		if (!isset($_SESSION[C('USER_AUTH_KEY')])) {
			$this->redirect('Admin/Login/index');
		}

		$not_auth = in_array(CONTROLLER_NAME, explode(',', C('NOT_AUTH_MODULE'))) || in_array(ACTION_NAME, explode(',', C('NOT_AUTH_FUNCTION'))); 
		// 判断是否开启登陆验证
		if (C('USER_AUTH_ON') && !$not_auth) {
			Rbac::AccessDecision() || $this->error('没有权限进行操作');
		}
	}
}

?>
