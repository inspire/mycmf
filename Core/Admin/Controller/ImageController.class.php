<?php
	
	namespace Admin\Controller;
	use Think\Controller;
	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 */
class ImageController extends CommonController{

		/**
		 * 
		 */
	public function view(){
			$this->display();
	}

		/**
		 * 修改Log
		 * @return [type] [description]
		 */
	public function changeLog(){
		$data = I('post.');
		$config = array('maxSize' => 3145728,
			'rootPath' => './Public/Uploads/Logs/',
			'exts' => array('jpg', 'gif', 'png', 'jpeg'),
			'subName' => array('date', 'Ymd'),
			);
		$upload = new \Think\Upload($config); // 实例化上传类
		
		$info = $upload->uploadOne($_FILES['imageurl']);
		if (!$info) {
			// 上传错误提示错误信息
			$this->error($upload->getError());
		}else {
			// 上传成功 获取上传文件信息
			$imageurl = $info['savepath'] . $info['savename'];
		}

		//修改上传图片的大小
		$imagename = './Public/Uploads/Logs/' . $imageurl;
		//实例化图像处理类
		$image = new \Think\Image();
		$image->open($imagename);
		$image->thumb(208,44,\THink\Image::IMAGE_THUMB_FIXED);
		$image->save($imagename);
		$data['imageurl'] = $imageurl;
		//删除旧的log图片
		$image_model = M('images');
		$origin = $image_model->where(array('type_id'=>$data['type_id']))->find();
		$data['id'] = $origin['id'];
		$imageurl = $origin['imageurl'];
		if ($imageurl) {
			$image_name = "./Public/Uploads/Logs/" . $imageurl;
			if($imageurl != "default.jpg"){
				unlink($image_name);
			}
		}
		//保存新的log
		$data['create_time'] = date("Y-m-d H:i:s");
		$data['update_time'] = date("Y-m-d H:i:s");
		$result = $image_model->save($data);
		if($result){
			$this->success("修改成功",U('Admin/Image/view'));
		}else{
			$this->error("修改失败");
		}
	}

		/**
		 * 修改幻灯图片,数据库中保证起初有且仅有2条记录，加一条，删一条
		 * @return [type] [description]
		 */
		
	public function changeBackground(){

		$config = array('maxSize' => 3145728,
			'rootPath' => './Public/Uploads/Backgrounds/',
			'exts' => array('jpg', 'gif', 'png', 'jpeg'),
			'subName' => array('date', 'Ymd'),
			);
		$upload = new \Think\Upload($config); // 实例化上传类
		$info = $upload->uploadOne($_FILES['imageurl']);
		if (!$info) {
			// 上传错误提示错误信息
			$this->error($upload->getError());
		}else {
			// 上传成功 获取上传文件信息
			$imageurl = $info['savepath'] . $info['savename'];
		}

		//修改上传图片的大小
		$imagename = './Public/Uploads/Backgrounds/' . $imageurl;
		//实例化图像处理类
		$image = new \Think\Image();
		$image->open($imagename);
		$image->thumb(1650,414,\THink\Image::IMAGE_THUMB_FIXED);
		$image->save($imagename);

		$image_model = M('images');
		$data = I('post.');
		$data['imageurl'] = $imageurl;
		$data['create_time'] = date("Y-m-d H:i:s");
		$data['update_time'] = date("Y-m-d H:i:s");

		$result = $image_model->add($data);
		if($result){
			//删除最旧的一条记录
			$old = $image_model->where(array('type_id'=>2))->order('update_time')->find();
			$imageurl = $old['imageurl'];
			$id = $old['id'];
			if ($imageurl) {
				$image_name = "./Public/Uploads/Backgrounds/" . $imageurl;
				if($imageurl != "default.jpg"){
					unlink($image_name);
				}
			}
			$image_model->where(array('id'=>$id))->delete();
			$this->success("修改成功",U('Admin/Image/view'));
		} else{
			$this->error("修改失败");
		}
	}

		/**
		 * 修改二维码图片
		 */

	public function changeQRCode(){

		$data = I('post.');
		$config = array('maxSize' => 3145728,
			'rootPath' => './Public/Uploads/QRCodes/',
			'exts' => array('jpg', 'gif', 'png', 'jpeg'),
			'subName' => array('date', 'Ymd'),
			);
		$upload = new \Think\Upload($config); // 实例化上传类
		
		$info = $upload->uploadOne($_FILES['imageurl']);
		if (!$info) {
			// 上传错误提示错误信息
			$this->error($upload->getError());
		}else {
			// 上传成功 获取上传文件信息
			$imageurl = $info['savepath'] . $info['savename'];
		}

		//修改上传图片的大小
		$imagename = './Public/Uploads/QRCodes/' . $imageurl;
		//实例化图像处理类
		$image = new \Think\Image();
		$image->open($imagename);
		$image->thumb(98,98,\THink\Image::IMAGE_THUMB_FIXED);
		$image->save($imagename);
		$data['imageurl'] = $imageurl;
		//删除旧的qrcode图片
		$image_model = M('images');
		$origin = $image_model->where(array('type_id'=>$data['type_id']))->find();
		$data['id'] = $origin['id'];
		$imageurl = $origin['imageurl'];
		if ($imageurl) {
			$image_name = "./Public/Uploads/QRCodes/" . $imageurl;
			if($imageurl != "default.jpg"){
				unlink($image_name);
			}
		}
		//保存新的log
		$data['create_time'] = date("Y-m-d H:i:s");
		$data['update_time'] = date("Y-m-d H:i:s");
		$result = $image_model->save($data);
		if($result){
			$this->success("修改成功",U('Admin/Image/view'));
		}else{
			$this->error("修改失败");
		}
	}
}

?>