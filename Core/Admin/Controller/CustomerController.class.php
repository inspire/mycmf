<?php
	
namespace Admin\Controller;
use Think\Controller;

/**
 * @author dongtian <dongtian@nuaa.edu.cn>
 * 客户信息管理控制器
 */
 class CustomerController extends CommonController
 {
 		
 		/**
 		 * 所有顾客信息的显示输出
 		 * @return [type] [description]
 		 */
 		public function listView(){
 			$this->title = '顾客信息列表';
 			$customer_model = M('customers');
 			$p = I('get.p',1,'intval');
 			$list = $customer_model->page($p . ',8')->select();
 			$count = $customer_model->count();

 			//实例化分页类
 			$Page = new \Think\Page($count,8);
 			$Page->setConfig('header', '个顾客');
			$Page->setConfig('prev', '上一页');
			$Page->setConfig('next', '下一页');
			$Page->setConfig('first', '第一页');
			$Page->setConfig('last', '最后页');
			$show = $Page->show(); // 分页显示输出
			$this->customers = $list; // 赋值
			$this->page = $show; // 赋值分页输出
			
			$this->display();

 		}

 		/**
 		 * 单个顾客信息的输出
 		 * @return [type] [description]
 		 */
 		public function view(){
 			$this->title = '顾客信息';
 			$id = I('id', '', 'intval');
			$map['id'] = $id;
			$customer_model = M('customers');
			$this->customer = $customer_model->where($map)->find();
			$this->display();
 		}
 		/**
 		 * 添加顾客信息
 		 */
 		public function add(){
 			$this->title = '添加顾客信息';
 			$this->display();
 		}
 		/**
 		 * 执行添加
 		 */
 		public function addDo(){
 			$customer_model = M('customers');
 			$customer_model->create();
 			$result = $customer_model->add();
 			if($result){
 				$this->success("添加成功", U('Admin/Customer/listView'));
 			}else{
 				$this->error('添加失败');
 			}
 		}
 		/**
 		 * 顾客信息修改
 		 * @return [type] [description]
 		 */
 		public function edit(){
 			$this->title = '修改顾客信息';
 			$id = I('get.id','','intval');
 			$map['id'] = $id;
 			$this->customer = M('customers')->where($map)->find();
 			$this->display();
 		}
 		/**
 		 * 执行修改
 		 * @return [type] [description]
 		 */
 		public function editDo(){
 			$id = I('post.id','','intval');
 			$map['id'] = $id;
 			$data = $_POST;
 			$customer_model = M('customers');
 			$result = $customer_model->where($map)->save($data);
 			if($result){
 				$this->success("修改成功", U('Admin/Customer/listView'));
 			}else{
 				$this->error('修改失败');
 			}
 		}
 		/**
 		 * 顾客信息删除
 		 * @return [type] [description]
 		 */
 		public function delete(){
 			$id = I('id','','intval');
 			$where['id'] = $id;
 			$customer_model = M('customers');
 			$result = $customer_model->where($where)->delete();
 			if($result){
 				$this->success("删除成功", U('Admin/Customer/listView'));
 			}else{
 				$this->error('删除失败');
 			}
 		}
 	
 } 

?>