<?php
	namespace Admin\Controller;
	use Think\Controller;
	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 子菜单控制器
	 */
	
	class SubmenuController extends CommonController {
		/**
		 * 子菜单listView
		 */
		public  function listView (){
			
			$submenu_model = D('SubmenuView');
			$count = $submenu_model->count();
			$p = I('get.p',1,'intval');//页码参数获取
			$submenus = $submenu_model->page($p . ',6')->select();
			$Page = new \Think\Page($count,6);
			$Page->setConfig('header','个菜单');
			$Page->setConfig('prev','上一页');
			$Page->setConfig('next','下一页');
			$Page->setConfig('first','第一页');
			$Page->setConfig('last','最后页');
			$this->title = '子菜单列表展示';
			$this->submenus = $submenus;
			$page = $Page->show();
			$this->page = $page;
			$this->display();
		}

		/**
		 * 添加子菜单view
		 */
		public function add(){
			$this->title = '添加子菜单';
			$this->types = M('types_submenu')->select();
			$this->display();
		}

		/**
		 * 执行添加,确保每种类型的菜单只有一个
		 */
		public function addDo(){
			if(IS_POST){	
				$data = I('post.');
				$where['type_id'] = $data['type_id'];
				$result = M('submenus')->where($where)->find();
				//菜单类型不存在则进行添加
				if(!$result){
					$result = M('submenus')->add($data);
					if($result){
						$this->success('添加成功',U('Admin/Submenu/listView'));
					}else{
						$this->error('添加失败');
					}
				}else{
					$this->error('该类型子菜单已经存在，请选择其他类型');
				}
			}else{
				$this->error('页面不存在');
			}
		}

		/**
		 * 编辑页面,设置不可修改菜单类型
		 */
		public function edit(){
			$submenu_model = D('SubmenuView');
			$this->title = '子菜单编辑';
			$id = I('get.id','','intval');
			$map['id'] = $id;
			$this->submenu = $submenu_model->where($map)->find();
			$this->display();
		}

		/**
		 * 执行修改
		 */
		public function editDo(){
			$data = I('post.');
			$result = M('submenus')->save($data);
			if($result){
				$this->success('修改成功',U('Admin/Submenu/listView'));
			}else{
				$this->error('修改失败');
			}
		}

		/**
		 * 菜单删除
		 */
		public function delete(){
			$id = I('get.id','','intval');
			$map['id'] = $id;
			$result = M('submenus')->where($map)->delete();
			if($result){
				$this->success('删除成功',U('Admin/Submenu/listView'));
			}else{
				$this->error('删除失败');
			}
		}
	}


?>