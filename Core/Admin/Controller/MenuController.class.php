<?php
	namespace Admin\Controller;
	use Think\Controller;

	/**
	 * @author [dongtian] <dongtian@nuaa.edu.cn>
	 * 菜单管理控制器
	 */

	class MenuController extends CommonController {

		/**
		 * 菜单展示(显示menus表中所有存在的记录)
		 */
		public function listView(){

			$menu_model = D('MenuRelation');
			$count = $menu_model->relation('menu_type')->count();
			$p = I('get.p',1,'intval');//页码参数获取
			$menus = $menu_model->relation('menu_type')->page($p . ',8')->select();
			$Page = new \Think\Page($count,8);
			$Page->setConfig('header','个菜单');
			$Page->setConfig('prev','上一页');
			$Page->setConfig('next','下一页');
			$Page->setConfig('first','第一页');
			$Page->setConfig('last','最后页');
			$this->title = '菜单列表展示';
			$this->menus = $menus;
			$page = $Page->show();
			$this->page = $page;
			$this->display();
		}

		/**
		 * 添加菜单view
		 */
		public function add(){
			$this->title = '添加菜单';
			$this->types = M('types_menu')->select();
			$this->display();
		}

		/**
		 * 执行添加,确保每种类型的菜单只有一个
		 */
		public function addDo(){
			
			if(IS_POST){	
				$data = I('post.');
				$where['type_id'] = $data['type_id'];
				$result = M('menus')->where($where)->find();
				//菜单类型不存在则进行添加
				if(!$result){
					$result = M('menus')->add($data);
					if($result){
						$this->success('添加成功',U('Admin/Menu/listView'));
					}else{
						$this->error('添加失败');
					}
				}else{
					$this->error('该类型菜单已经存在，请选择其他类型');
				}
			}else{
				$this->error('页面不存在');
			}
		}

		/**
		 * 编辑页面,设置不可修改菜单类型
		 */
		public function edit(){
			$this->title = '菜单编辑';
			$id = I('get.id','','intval');
			$map['id'] = $id;
			$this->menu = D('MenuRelation')->relation('menu_type')->where($map)->find();
			$this->display();
		}

		/**
		 * 执行修改
		 */
		public function editDo(){
			$data = I('post.');
			$result = M('menus')->save($data);
			if($result){
				$this->success('修改成功',U('Admin/Menu/listView'));
			}else{
				$this->error('修改失败');
			}
		}

		/**
		 * 菜单删除
		 */
		public function delete(){
			$id = I('get.id','','intval');
			$map['id'] = $id;
			$result = M('menus')->where($map)->delete();
			if($result){
				$this->success('删除成功',U('Admin/Menu/listView'));
			}else{
				$this->error('删除失败');
			}
		}
	}

?>