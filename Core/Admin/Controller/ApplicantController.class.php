<?php
	
namespace Admin\Controller;
use Think\Controller;

/**
 * @author dongtian <dongtian@nuaa.edu.cn>
 * 客户信息管理控制器
 */
 class ApplicantController extends CommonController
 {
 		
 		/**
 		 * 所有应聘者信息的显示输出
 		 * @return [type] [description]
 		 */
 		public function listView(){
 			$this->title = '应聘者信息列表';
 			$applicant_model = M('applicants');
 			$p = I('get.p',1,'intval');
 			$list = $applicant_model->page($p . ',8')->select();
 			$count = $applicant_model->count();

 			//实例化分页类
 			$Page = new \Think\Page($count,8);
 			$Page->setConfig('header', '个应聘者');
			$Page->setConfig('prev', '上一页');
			$Page->setConfig('next', '下一页');
			$Page->setConfig('first', '第一页');
			$Page->setConfig('last', '最后页');
			$show = $Page->show(); // 分页显示输出
			$this->applicants = $list; // 赋值
			$this->page = $show; // 赋值分页输出
			
			$this->display();

 		}

 		/**
 		 * 单个应聘者信息的输出
 		 * @return [type] [description]
 		 */
 		public function view(){
 			$this->title = '应聘者信息';
 			$id = I('id', '', 'intval');
			$map['id'] = $id;
			$applicant_model = M('applicants');
			$this->applicant = $applicant_model->where($map)->find();
			$this->display();
 		}
 		/**
 		 * 添加应聘者信息
 		 */
 		public function add(){
 			$this->title = '添加应聘者信息';
 			$this->display();
 		}
 		/**
 		 * 执行添加
 		 */
 		public function addDo(){
 			$applicant_model = M('applicants');
 			$data = I('post.');
 			$config = array('maxSize' => 3145728,
			'rootPath' => './Public/Uploads/Resumes/',
			'exts' => array('doc', 'docx', 'pdf', 'txt','rar','zip'),
			'subName' => array('date', 'Ymd'),
			);
			//有文件上传
			if($_FILES['resumeurl']['name'] != null)	{

				$upload = new \Think\Upload($config); // 实例化上传类
				$info = $upload->uploadOne($_FILES['resumeurl']);
				if (!$info) {
					// 上传错误提示错误信息
					$this->error($upload->getError());
				}else {
					// 上传成功 获取上传文件信息
					$resumeurl = $info['savepath'] . $info['savename'];
					$data['resumeurl'] = $resumeurl;
				}
			}
 			$result = $applicant_model->add($data);
 			if($result){
 				$this->success("添加成功", U('Admin/Applicant/listView'));
 			}else{
 				$this->error('添加失败');
 			}
 		}
 		/**
 		 * 顾客信息修改
 		 * @return [type] [description]
 		 */
 		public function edit(){
 			$this->title = '修改应聘者信息';
 			$id = I('get.id','','intval');
 			$map['id'] = $id;
 			$this->applicant = M('applicants')->where($map)->find();
 			$this->display();
 		}
 		/**
 		 * 执行修改
 		 * @return [type] [description]
 		 */
 		public function editDo(){
 			$id = I('post.id','','intval');
 			$map['id'] = $id;
 			$data = $_POST;
 			$applicant_model = M('applicants');
 			$config = array('maxSize' => 3145728,
			'rootPath' => './Public/Uploads/Resumes/',
			'exts' => array('doc', 'docx', 'pdf', 'txt','rar','zip'),
			'subName' => array('date', 'Ymd'),
			);
			//有文件上传
			if($_FILES['resumeurl']['name'] != null)	{

				$upload = new \Think\Upload($config); // 实例化上传类
				$info = $upload->uploadOne($_FILES['resumeurl']);
				if (!$info) {
					// 上传错误提示错误信息
					$this->error($upload->getError());
				}else {
					//删除旧的简历
					$resumeurl = $applicant_model->where($map)->getField('resumeurl');
					$file_name = "./Public/Uploads/Resumes/" . $resumeurl; 
					unlink($file_name);
					// 上传成功 获取上传文件信息
					$resumeurl = $info['savepath'] . $info['savename'];
					$data['resumeurl'] = $resumeurl;
				}
			}
 			$result = $applicant_model->save($data);
 			if($result){
 				$this->success("修改成功", U('Admin/Applicant/listView'));
 			}else{
 				$this->error('修改失败');
 			}
 		}
 		/**
 		 * 应聘者信息删除
 		 * @return [type] [description]
 		 */
 		public function delete(){
 			$id = I('id','','intval');
 			$where['id'] = $id;
 			$applicant_model = M('applicants');
 			$resumeurl = $applicant_model->where($where)->getField('resumeurl');
			if ($resumeurl) {
				$file_name = "./Public/Uploads/Resumes/" . $resumeurl;
					unlink($file_name);
			}
 			$result = $applicant_model->where($where)->delete();
 			if($result){
 				$this->success("删除成功", U('Admin/Applicant/listView'));
 			}else{
 				$this->error('删除失败');
 			}
 		}	
 } 

?>