<?php
	namespace Admin\Model;
	use Think\Model\RelationModel;
	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 文件与文件种类关联模型
	 */
	class FileRelationModel extends RelationModel{
		//定义主表
		protected $tableName = 'files';
		//定义关联模型
		protected $_link = array(
			//副表
			'types_file' => array(
				'mapping_type' => self::BELONGS_TO,
				'mapping_name' => 'file_type',
				'foreign_key' => 'type_id',
				'mapping_fields' => 'id,name',
				),
			);
	}
?>