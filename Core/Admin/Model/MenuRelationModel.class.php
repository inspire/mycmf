<?php
	namespace Admin\Model;
	use Think\Model\RelationModel;

	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 菜单  与 菜单类型  关联模型
	 */
	
	class MenuRelationModel extends RelationModel{
		//定义主表
		protected $tableName = 'menus';
		//定义关联条件
		protected $_link =array(

			'types_menu' => array(
				'mapping_type' => self::BELONGS_TO,
				'mapping_name' => 'menu_type',
				'foreign_key' => 'type_id',
				'mapping_fields' => 'id,name',
				),
			);
	}
?>