<?php
	namespace Admin\Model;
	use Think\Model\RelationModel;
	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 产品与种类关联模型
	 */
	class ProductRelationModel extends RelationModel{
		//定义主表
		protected $tableName = 'products';
		//定义关联模型
		protected $_link = array(
			//副表
			'categories' => array(
				'mapping_type' => self::BELONGS_TO,
				'mapping_name' => 'product_type',
				'foreign_key' => 'type_id',
				'mapping_fields' => 'id,name,is_active',
				),
			);
	}
?>