<?php
	namespace Admin\Model;
	use Think\Model\RelationModel;
	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 文章 与 文章类别  关联模型
	 */
	class ArticleRelationModel extends RelationModel{
		//定义主表
		protected $tableName = 'articles';
		//定义关联模型
		protected $_link = array(

			'types_article' => array(
				'mapping_type' => self::BELONGS_TO,
				'mapping_name' => 'article_type',
				'foreign_key' => 'type_id',
				'mapping_fields'=> 'id,name',
				),
			);
	}


?>