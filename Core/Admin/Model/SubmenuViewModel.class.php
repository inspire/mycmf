<?php
	namespace Admin\Model;
	use Think\Model\ViewModel;
	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 子菜单视图模型
	 */
	class SubmenuViewModel extends ViewModel {

		public $viewFields =  array(
			'submenus' => array('id','type_id','name','sort_order','is_active'),
			'types_submenu' => array('name'=>'type_name_submenu','parent_id','_on'=>'types_submenu.id=submenus.type_id'),
			'types_menu' => array('name'=>'type_name_menu','_on'=>'types_menu.id=types_submenu.parent_id'),
			'menus' => array('name'=>'name_menu','is_nav','nav_order','is_foot','foot_order','_on'=>'menus.type_id=types_menu.id'),
			);
	}


?>