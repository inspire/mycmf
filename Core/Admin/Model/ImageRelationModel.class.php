<?php
	namespace Admin\Model;
	use Think\Model\RelationModel;
	/**
	 * @author dongtian <dongtian@nuaa.edu.cn>
	 * 图片与图片种类关联模型
	 */
	class ImageRelationModel extends RelationModel{
		//定义主表
		protected $tableName = 'images';
		//定义关联模型
		protected $_link = array(
			//副表
			'types_images' => array(
				'mapping_type' => self::BELONGS_TO,
				'mapping_name' => 'image_type',
				'foreign_key' => 'type_id',
				'mapping_fields' => 'id,name',
				),
			);
	}
?>