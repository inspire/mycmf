<?php 
// 类似于var_dump作用，将数组，变量格式化输出
function p($array) {
	dump($array, true, '<pre>', false);
}
// 两数比大小
function bigSmall($a, $b) {
	if ($a > $b) {
		return $a;
	}else {
		return $b;
	}
}
// 清除多余的空白，包括制表符，换行，回车
function cleanSpace($str) {
	// 清除两端空白
	$str_first = trim($str); 
	// 清除多余重复的空格
	$str_second = preg_replace('/\s(?=\s)/', '', $str_first); 
	// 清除制表符和换行回车
	$str_final = preg_replace('/[\n\r\t]/', ' ', $str_second);

	return $str_final;
}

/**
 * 截取中文字符串
$str传入的字符串
 * $length截取的长度
$suffix 是否添加省略号
 */
function msubstr($str, $start = 0, $length, $charset = "utf-8", $suffix = true) {
	$str_len = strlen($str) / 3;
	if (function_exists("mb_substr")) {
		if ($str_len > $length) {
			if ($suffix) {
				return mb_substr($str, $start, $length, $charset) . "...";
			}else {
				return mb_substr($str, $start, $length, $charset);
			}
		}else {
			return $str;
		}
	}elseif (function_exists('iconv_substr')) {
		if ($str_len > $length) {
			if ($suffix) {
				return iconv_substr($str, $start, $length, $charset) . "...";
			}else {
				return iconv_substr($str, $start, $length, $charset);
			}
		}else {
			return $str;
		}
	}
	$re['utf-8'] = "/[x01-x7f]|[xc2-xdf][x80-xbf]|[xe0-xef][x80-xbf]{2}|[xf0-xff][x80-xbf]{3}/";
	$re['gb2312'] = "/[x01-x7f]|[xb0-xf7][xa0-xfe]/";
	$re['gbk'] = "/[x01-x7f]|[x81-xfe][x40-xfe]/";
	$re['big5'] = "/[x01-x7f]|[x81-xfe]([x40-x7e]|xa1-xfe])/";
	preg_match_all($re[$charset], $str, $match);

	$slice = join("", array_slice($match[0], $start, $length));
	if ($str_len > $length) {
		if ($suffix) {
			return $slice . "…";
		}else {
			return $slice;
		}
	}else {
		return $str;
	}
}

/**
 * 实现标题的准确大写,
 * 原理：先将所有单词首字母转换大写，再将特殊单词转为小写并排除第一个和最后一个单词
 */

function getTitleUpcase($str) {
	$str = ucwords($str); 
	// 将字符串以数组key=》value形式，第二种格式
	$wordlist = str_word_count($str, 2);
	$wordlist = array_slice($wordlist, 1, -1, true);
	foreach ($wordlist as $key => $value) {
		switch ($value) {
			case 'A':
			case 'An':
			case 'The':
			case 'But':
			case 'As':
			case 'If':
			case 'And':
			case 'Or':
			case 'Nor':
			case 'Of':
			case 'By': 
				// 首字母再改为小写
				$lower = strtolower($value); 
				// 相对应的str的key
				$str[$key] = $lower[0];
		}
	}
	return $str;
}

/**
 * * 月份显示
 * 
 * @param int $m 1-12
 * @param int $type 0:long 1:short(default) 2:chinese
 * @return String 
 */
function getFormatMonth($m, $type = 1) {
	$month = array(
		array('', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
		array('', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'),
		array('', '一月', '二月', '三月', '四月', '五月', '六月', '七月', '八月', '九月', '十月', '十一月', '十二月')
		);
	return $month[$type][$m];
}

/**
 * * 延时输出内容
 * 
 * @param int $sec 秒数，可以是小数例如 0.3
 */
function cache_flush($sec = 2) {
	ob_flush();
	flush();
	usleep($sec * 1000000);
}

/**
 * * 获取文件或文件夹的拥有者,组用户,及权限
 * 
 * @param String $filename 
 * @return Array 
 */
function file_attribute($filename) {
	if (!file_exists($filename)) {
		return false;
	}

	$owner = posix_getpwuid(fileowner($filename));
	$group = posix_getpwuid(filegroup($filename));
	$perms = substr(sprintf('%o', fileperms($filename)), -4);

	$ret = array('owner' => $owner['name'],
		'group' => $group['name'],
		'perms' => $perms
		);

	return $ret;
}

?>
